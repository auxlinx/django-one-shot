from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class TodoListForm(ModelForm):
    # email_address = forms.EmailField(max_length=300)

    class Meta:
        model = TodoList
        fields = "__all__"


class TodoItemForm(ModelForm):
    # email_address = forms.EmailField(max_length=300)

    class Meta:
        model = TodoItem
        fields = "__all__"
