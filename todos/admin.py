from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.


@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
        # "description",
        # "created_on",
        # "updated_on",
    ]


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "due_date",
        "is_completed",
        "list",
        # "updated_on",
    ]
