from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm, TodoItemForm


def todo_lists(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_lists.html", context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todolist,
    }
    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
        # model_instance = form.save(commit=False)
        # model_instance.user = request.user
        # model_instance.save()
        # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    update_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update_list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            update_list = form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance=update_list)

    context = {"form": form, "update_list": update_list}

    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    delete_todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_todo_list.delete()
        return redirect("todo_lists")

    return render(request, "todos/todo_list_delete.html")


def todo_item_create(request, id):
    add_item = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=add_item)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            add_item = form.save()
            return redirect("todo_list_detail", id=add_item.list.id)
    else:
        form = TodoItemForm(instance=add_item)

    context = {"form": form, "add_item": add_item}

    return render(request, "todos/todo_item_create.html", context)
